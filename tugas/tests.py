from django.test import TestCase
from .models import *
from django.urls import resolve
from .views import *
from .forms import *

class unitTests(TestCase):

    def test_url_exists(self):
        
        response = self.client.get("/")

        self.assertEqual(response.status_code,200)

        self.assertTemplateUsed(response,"pages/index.html")

        self.assertContains(response, "Please log in")

    def test_function_index(self):
        found = resolve("/")
        self.assertEqual(found.func, index)

    def test_register_url(self):

        response = self.client.get("/register")

        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, "pages/register.html")

    def test_function_register(self):

        found = resolve("/register")

        self.assertEqual(found.func, register)

    def test_post_register_form(self):
        response = self.client.post('/register',{'username':'username','password1':'password123','password2':'password123','email':'email@email.com','first_name':'first','last_name':'last','image':'https://miro.medium.com/max/3000/1*8qF4xvOX0z1gye6BczfGQQ.png'}, follow = True)

        html_response = response.content.decode('utf8')
        
        self.assertEqual(response.status_code,200)

# Create your tests here.
