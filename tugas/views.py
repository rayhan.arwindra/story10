from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from .forms import RegistrationForm
from .models import Picture
from django.contrib.auth.models import User
# Create your views here.
def index(request):
    context = {}
    if request.user.is_authenticated:
        context['image'] = Picture.objects.get(user = request.user)
    return render(request,'pages/index.html', context)

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()

            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']

            image = request.POST['image']
            user = User.objects.get(username=username)

            userPicture = Picture.objects.create(picture=image,user=user)
            userPicture.save()

            user = authenticate(username = username, password = password)
            login(request, user)
            return redirect('index')
    else:
        form = RegistrationForm()
    return render(request, 'pages/register.html',{'form' : form})