from django.db import models
from django.conf import settings

class Picture(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    picture = models.ImageField('/')

    def __str__(self):
        return str(self.user)
# Create your models here.